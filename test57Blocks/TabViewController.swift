//
//  ViewController.swift
//  test57Blocks
//
//  Created by Carlos on 3/09/21.
//

import UIKit

class TabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let homeVC = HomeViewController.assembleModule()
        homeVC.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 0)
        
        let favoritesVC = FavoritesViewController.assembleModule()
        favoritesVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
        
        let viewControllerList = [ homeVC, favoritesVC ]
        viewControllers = viewControllerList
    
    }


}

