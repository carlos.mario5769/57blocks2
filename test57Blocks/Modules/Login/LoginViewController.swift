//
//  LoginViewController.swift
//  test57Blocks
//
//  Created by Carlos on 3/09/21.
//

import UIKit
import Combine

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    
    private let viewModel = LoginViewModel()
    private var bindings = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTargets()
        bindViewToViewModel()
        bindViewModelToView()
        viewModel.checkIfIsLogged()
    }
    
    private func setUpTargets() {
        loginButton.addTarget(self, action: #selector(loginButtonDidClick), for: .touchUpInside)
    }

    private func bindViewToViewModel() {
        emailTextField.textPublisher
            .receive(on: DispatchQueue.main)
            .assign(to: \.email, on: viewModel)
            .store(in: &bindings)
        
        passwordTextField.textPublisher
            .receive(on: DispatchQueue.main)
            .assign(to: \.password, on: viewModel)
            .store(in: &bindings)
    }
    
    private func bindViewModelToView() {
        viewModel.areInputPresent
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.loginButton.isEnabled = $0
                self.loginButton.backgroundColor = $0 ? UIColor.red : UIColor.gray
            }
            .store(in: &bindings)
        
        viewModel.validationPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.emailErrorLabel.text = $0.emailError ?? ""
                self.passwordErrorLabel.text = $0.passwordError ?? ""
                if $0.isValid {
                    presentHome()
                }
            }
            .store(in: &bindings)
        
        viewModel.isAlreadyLoggedPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                if $0 {
                    self.presentHome()
                }
            }
            .store(in: &bindings)
    }
    
    @objc private func loginButtonDidClick() {
        viewModel.validateCredentials()
    }
    
    private func presentHome() {
        let tabVC = TabViewController()
        tabVC.modalPresentationStyle = .fullScreen
        present(tabVC, animated: true, completion: nil)
    }
}
