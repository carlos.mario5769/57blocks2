//
//  LoginViewModel.swift
//  test57Blocks
//
//  Created by Carlos on 3/09/21.
//

import Foundation
import Combine

class LoginViewModel {
    
    @Published var email: String = ""
    @Published var password: String = ""
    let isAlreadyLoggedPublisher: CurrentValueSubject<Bool, Never> = .init(false)
    let validationPublisher: PassthroughSubject<LoginResultVM, Never> = PassthroughSubject()
    let defaults = UserDefaults.standard
    let isLoggedDefault = "isLoggedDefault"
    
    private(set) lazy var areInputPresent = Publishers.CombineLatest($email, $password)
        .map { $0.count >= 0 && $1.count >= 0 }
        .eraseToAnyPublisher()
    
    func validateCredentials() {
        let emailError = !isValidEmail() ? "Formato de correo incorrecto" : nil
        let passwordError = !isValidPassword() ? "La contraseña debe tener mínimo una letra en mayúscula, una letra en minúscula, un número y un caracter especial" : nil
        let vm = LoginResultVM(emailError: emailError, passwordError: passwordError)
        if vm.isValid {
            defaults.set(true, forKey: isLoggedDefault)
        }
        validationPublisher.send(vm)
    }
    
    func checkIfIsLogged() {
        isAlreadyLoggedPublisher.send(defaults.bool(forKey: isLoggedDefault))
    }
    
    private func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    private func isValidPassword() -> Bool {
        let passwordRegEx = ".*[^a-z0-9A-Z].*"
        let passwordPred = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordPred.evaluate(with: password)
    }
}
