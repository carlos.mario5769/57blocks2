//
//  LoginErrorVM.swift
//  test57Blocks
//
//  Created by Carlos on 3/09/21.
//

import Foundation

struct LoginResultVM {
    var emailError: String?
    var passwordError: String?
    
    
    var isValid: Bool {
        get {
            emailError == nil && passwordError == nil
        }
    }
}
