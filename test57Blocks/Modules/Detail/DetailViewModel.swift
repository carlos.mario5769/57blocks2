//
//  DetailViewModel.swift
//  test57Blocks
//
//  Created by Carlos on 6/09/21.
//

import Foundation
import Combine

class DetailViewModel {
    
    let interactor = DetailInteractor()
    var item: ItemVM
    let speciesPublisher: PassthroughSubject<String, Never> = PassthroughSubject()
    let typesPublisher: PassthroughSubject<String, Never> = PassthroughSubject()
    let abilitiesPublisher: PassthroughSubject<String, Never> = PassthroughSubject()
    let imagePublisher: PassthroughSubject<String, Never> = PassthroughSubject()
    let isFavoritePublisher: CurrentValueSubject<Bool, Never> = .init(false)
    
    private var bindings = Set<AnyCancellable>()
    
    init(item: ItemVM) {
        self.item = item
        interactor.dataPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                if case .success(let result, _) = $0 {
                    let typesString = result.types?.reduce("") {
                        (result, next) -> String in
                        return "\(result)\(result.count > 0 ? " " : "")\(next.type?.name?.capitalized ?? "")"
                        }
                    let abilitiesString = result.abilities?.reduce("") {
                        (result, next) -> String in
                        return "\(result)\(result.count > 0 ? ", " : "")\(next.ability?.name?.capitalized ?? "")\(next.isHidden == true ? " (Oculta)" : "")"
                    }
                    self.speciesPublisher.send(result.species?.name?.capitalized ?? "")
                    self.typesPublisher.send(typesString ?? "")
                    self.abilitiesPublisher.send(abilitiesString ?? "")
                    self.imagePublisher.send(result.sprites?.frontDefault ?? "")
                }
            }
            .store(in: &bindings)
    }
    
    func getDetail() {
        interactor.getDetail(itemId: item.id)
        isFavoritePublisher.send(interactor.isFavorite(itemId: item.id))
    }
    
    func toggleFavorite() {
        interactor.toggleFavorite(item: item)
        isFavoritePublisher.send(!isFavoritePublisher.value)
    }
}

