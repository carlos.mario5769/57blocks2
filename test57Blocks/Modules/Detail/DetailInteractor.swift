//
//  DetailInteractor.swift
//  test57Blocks
//
//  Created by Carlos on 6/09/21.
//

import Foundation
import Combine
import CoreData

class DetailInteractor {
    
    private let manager = CoreDataManager()
    
    let dataPublisher: PassthroughSubject<ApiResult<ApiResponse.PokemonDetail>, Never> = PassthroughSubject()
    
    func getDetail(itemId: String) {
        let request = Endpoint.GetPokemonDetail(pokemonId: itemId)
        ApiClient.sharedInstance.doRequest(req: request) { [unowned self] (apiResult: ApiResult<ApiResponse.PokemonDetail>) in
            dataPublisher.send(apiResult)
        }
    }
    
    func isFavorite(itemId: String) -> Bool {
        return getItem(itemId: itemId) != nil
    }
    
    func toggleFavorite(item: ItemVM) {
        
        let context = manager.container.viewContext
        
        if let favorite = getItem(itemId: item.id) {
            context.delete(favorite)
        } else {
            let favorite = Favorites(context: context)
            favorite.name = item.name
            favorite.id = item.id
        }
        do {
            try context.save()
        } catch {
            fatalError("El error obteniendo usuario(s) \(error)")
        }
    }
    
    private func getItem(itemId: String) -> Favorites? {
        
        let fetchRequest : NSFetchRequest<Favorites> = Favorites.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", itemId)
        
        do {
            let result = try manager.container.viewContext.fetch(fetchRequest)
            return result.first
        } catch {
            fatalError("El error obteniendo información \(error)")
        }
    }
}
