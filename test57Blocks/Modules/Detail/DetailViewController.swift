//
//  DetailViewController.swift
//  test57Blocks
//
//  Created by Carlos on 3/09/21.
//

import UIKit
import Combine
import AlamofireImage

class DetailViewController: UIViewController {

    @IBOutlet weak var favoriteImageView: UIImageView!
    @IBOutlet weak var spriteImageView: UIImageView!
    @IBOutlet weak var typesLabel: UILabel!
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var abilitiesLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!
    
    private var viewModel: DetailViewModel!
    private var bindings = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModelToView()
        viewModel.getDetail()
    }
    
    static func assembleModule(item: ItemVM) -> UIViewController {
        let viewController: DetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController()
        viewController.title = item.name
        let viewModel = DetailViewModel(item: item)
        viewController.viewModel = viewModel
        return viewController
    }

    private func bindViewModelToView() {
        viewModel.abilitiesPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.abilitiesLabel.text = $0
            }
            .store(in: &bindings)
        viewModel.speciesPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.speciesLabel.text = $0
            }
            .store(in: &bindings)
        viewModel.typesPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.typesLabel.text = $0
            }
            .store(in: &bindings)
        viewModel.imagePublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.spriteImageView.af.setImage(withURL: URL(string: $0)!)
            }
            .store(in: &bindings)
        viewModel.isFavoritePublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.favoriteImageView.image = UIImage(systemName: $0 ? "star.fill" : "star")
            }
            .store(in: &bindings)
    }
    
    @IBAction func favoriteButtonDidClick(_ sender: Any) {
        viewModel.toggleFavorite()
    }
}
