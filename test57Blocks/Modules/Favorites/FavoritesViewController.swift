//
//  FavoritesViewController.swift
//  test57Blocks
//
//  Created by Carlos on 3/09/21.
//

import UIKit
import Combine

class FavoritesViewController: UIViewController {

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = FavoritesViewModel()
    private var bindings = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        bindViewModelToView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getData()
    }
    
    private func bindViewModelToView() {
        
        viewModel.dataPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.loadingView.isHidden = true
                switch $0 {
                case .loading:
                    self.loadingView.isHidden = false
                case .finishedLoading:
                    self.tableView.reloadData()
                }
            }
            .store(in: &bindings)
    }
    
    static func assembleModule() -> UIViewController {
        let viewController: FavoritesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController()
        viewController.title = "Favoritos"
        let navigationController = UINavigationController(rootViewController: viewController)
        return navigationController
    }

}

extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ItemTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureCell(delegate: self, item: viewModel.items[indexPath.row])
        return cell
    }
}

extension FavoritesViewController: ItemTableViewCellProtocol {
    
    func itemDidSelect(item: ItemVM) {
        let detailVC = DetailViewController.assembleModule(item: item)
        detailVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
}
