//
//  FavoritesInteractor.swift
//  test57Blocks
//
//  Created by Carlos on 6/09/21.
//

import Foundation
import Combine
import CoreData

class FavoritesInteractor {
    
    private let manager = CoreDataManager()
    
    let dataPublisher: PassthroughSubject<[ItemVM], Never> = PassthroughSubject()
    
    func getItems() {
        
        let fetchRequest : NSFetchRequest<Favorites> = Favorites.fetchRequest()
        
        do {
            let result = try manager.container.viewContext.fetch(fetchRequest)
            dataPublisher.send(result.map { ItemVM(name: $0.name ?? "", id: $0.id ?? "") })
        } catch {
            print("El error obteniendo información \(error)")
        }
    }
}
