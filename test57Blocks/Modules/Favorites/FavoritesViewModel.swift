//
//  FavoritesViewModel.swift
//  test57Blocks
//
//  Created by Carlos on 6/09/21.
//

import Foundation
import Combine

enum FavoritesViewModelState {
    case loading
    case finishedLoading
}

class FavoritesViewModel {
    
    let interactor = FavoritesInteractor()
    
    var items: [ItemVM] = []
    
    let dataPublisher: PassthroughSubject<FavoritesViewModelState, Never> = PassthroughSubject()
    
    private var bindings = Set<AnyCancellable>()
    
    init() {
        
        interactor.dataPublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.items = $0
                dataPublisher.send(.finishedLoading)
            }
            .store(in: &bindings)
    }
    
    func getData() {
        dataPublisher.send(.loading)
        interactor.getItems()
    }
}


