//
//  HomeViewController.swift
//  test57Blocks
//
//  Created by Carlos on 3/09/21.
//

import UIKit
import Combine

class HomeViewController: UIViewController {

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var leftPageButton: UIButton!
    @IBOutlet weak var rightPageButton: UIButton!
    @IBOutlet weak var pagesLabel: UILabel!
    
    private let viewModel = HomeViewModel()
    private var bindings = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        bindViewModelToView()
        viewModel.getData()
        setUpTargets()
    }
    
    static func assembleModule() -> UIViewController {
        let viewController: HomeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController()
        viewController.title = "Home"
        let navigationController = UINavigationController(rootViewController: viewController)
        return navigationController
    }
    
    private func setUpTargets() {
        leftPageButton.addTarget(self, action: #selector(leftPageButtonDidClick), for: .touchUpInside)
        rightPageButton.addTarget(self, action: #selector(rightPageButtonDidClick), for: .touchUpInside)
    }
    
    private func bindViewModelToView() {
        viewModel.leftButtonIsHidden
            .receive(on: RunLoop.main)
            .assign(to: \.isHidden, on: leftPageButton)
            .store(in: &bindings)
        
        viewModel.rightButtonIsHidden
            .receive(on: RunLoop.main)
            .assign(to: \.isHidden, on: rightPageButton)
            .store(in: &bindings)
        
        viewModel.pageText
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.pagesLabel.text = $0
            }
            .store(in: &bindings)
        
        viewModel.homeStatePublisher
            .receive(on: RunLoop.main)
            .sink { [unowned self] in
                self.loadingView.isHidden = true
                switch $0 {
                case .loading:
                    self.loadingView.isHidden = false
                case .finishedLoading:
                    self.tableView.reloadData()
                case .error:
                    break
                }
            }
            .store(in: &bindings)
    }
    
    @objc private func leftPageButtonDidClick() {
        viewModel.previousPage()
    }
    
    @objc private func rightPageButtonDidClick() {
        viewModel.nextPage()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ItemTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureCell(delegate: self, item: viewModel.items[indexPath.row])
        return cell
    }
}

extension HomeViewController: ItemTableViewCellProtocol {
    
    func itemDidSelect(item: ItemVM) {
        let detailVC = DetailViewController.assembleModule(item: item)
        detailVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
}
