//
//  ItemVM.swift
//  test57Blocks
//
//  Created by Carlos on 6/09/21.
//

import Foundation

struct ItemVM {
    var name: String
    var id: String
    
    static func mapFromDomain(apiResponse: ApiResponse.PokemonItem) -> ItemVM {
        let split = apiResponse.url?.split(separator: "/").map(String.init)
        let id = split?.last ?? ""
        let name = apiResponse.name?.capitalized ?? ""
        return ItemVM(name: name, id: id)
    }
}
