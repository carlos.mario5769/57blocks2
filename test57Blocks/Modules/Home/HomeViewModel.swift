//
//  HomeViewModel.swift
//  test57Blocks
//
//  Created by Carlos on 5/09/21.
//

import Foundation
import Combine

enum HomeViewModelState {
    case loading
    case finishedLoading
    case error
}

class HomeViewModel {
    
    @Published var pageNumber: Int = 1
    @Published var totalPages: Int = 1
    let limit = 200
    
    private(set) lazy var pageText = Publishers.CombineLatest($pageNumber, $totalPages)
        .map { [unowned self ] in "Página \($0) de \($1)" }
        .eraseToAnyPublisher()
    
    private(set) lazy var leftButtonIsHidden = $pageNumber
        .map { $0 == 1 }
        .eraseToAnyPublisher()
    
    private(set) lazy var rightButtonIsHidden = Publishers.CombineLatest($pageNumber, $totalPages)
        .map { $0 == $1 }
        .eraseToAnyPublisher()
    
    var items: [ItemVM] = []
    
    let homeStatePublisher: PassthroughSubject<HomeViewModelState, Never> = PassthroughSubject()
    
    func getData() {
        homeStatePublisher.send(HomeViewModelState.loading)
        let offset = (pageNumber - 1) * limit
        let request = Endpoint.GetPokemonList(limit: limit, offset: offset)
        ApiClient.sharedInstance.doRequest(req: request) { [unowned self] (apiResult: ApiResult<ApiResponse.PokemonResult>) in
            if case .success(let result, _) = apiResult {
                self.totalPages = ((result.count ?? 0 + 1) / self.limit) + 1
                self.items = result.results?.map { ItemVM.mapFromDomain(apiResponse: $0) } ?? []
                homeStatePublisher.send(HomeViewModelState.finishedLoading)
            }
            else {
                homeStatePublisher.send(HomeViewModelState.error)
            }
        }
    }
    
    func previousPage() {
        if pageNumber > 1 {
            pageNumber -= 1
            getData()
        }
    }
    
    func nextPage() {
        if pageNumber < totalPages {
            pageNumber += 1
            getData()
        }
    }
    
}
