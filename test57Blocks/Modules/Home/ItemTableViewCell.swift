//
//  ItemTableViewCell.swift
//  test57Blocks
//
//  Created by Carlos on 5/09/21.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    var item: ItemVM?
    weak var delegate: ItemTableViewCellProtocol?
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(delegate: ItemTableViewCellProtocol, item: ItemVM) {
        self.delegate = delegate
        self.item = item
        nameLabel.text = item.name
    }

    @IBAction func itemDidClick(_ sender: Any) {
        guard let item = item else { return }
        delegate?.itemDidSelect(item: item)
    }
    
}

protocol ItemTableViewCellProtocol: class {
    func itemDidSelect(item: ItemVM)
}
