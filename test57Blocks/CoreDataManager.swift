//
//  CoreDataManager.swift
//  test57Blocks
//
//  Created by Carlos on 6/09/21.
//

import Foundation
import CoreData

class CoreDataManager {
    
    public let container : NSPersistentContainer!
    
    init() {
        container = NSPersistentContainer(name: "Model")
        
        setupDatabase()
    }
    
    private func setupDatabase() {
        
        container.loadPersistentStores { (desc, error) in
            if let error = error {
                print("Error loading store \(desc) — \(error)")
                return
            }
        }
    }
}
