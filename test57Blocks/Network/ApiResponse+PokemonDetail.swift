//
//  ApiResponse+PokemonDetail.swift
//  test57Blocks
//
//  Created by Carlos on 6/09/21.
//

import Foundation

extension ApiResponse {
    struct AbilityInfoName: Codable {
        var name: String?
    }
    
    struct AbilityInfo: Codable {
        var isHidden: Bool?
        var ability: AbilityInfoName?
        
        enum CodingKeys: String, CodingKey {
            case ability
            case isHidden = "is_hidden"
        }
    }
    
    struct SpeciesInfo: Codable {
        var name: String?
    }
    
    struct TypeInfoName: Codable {
        var name: String?
    }
    
    struct TypeInfo: Codable {
        var type: TypeInfoName?
    }
    
    struct Sprites: Codable {
        var frontDefault: String?
        
        enum CodingKeys: String, CodingKey {
            case frontDefault = "front_default"
        }
    }
    
    struct PokemonDetail: Codable {
        var types: [TypeInfo]?
        var abilities: [AbilityInfo]?
        var species: SpeciesInfo?
        var sprites: Sprites?
    }
}
