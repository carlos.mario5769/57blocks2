//
//  Endpoint+GetPokemonList.swift
//  test57Blocks
//
//  Created by Carlos on 5/09/21.
//

import Foundation
import Alamofire

extension Endpoint {
    
    struct GetPokemonList {
        let limit: Int
        let offset: Int
    }
}

extension Endpoint.GetPokemonList: EndpointProtocol {

    var path: String { return "pokemon" }
    
    var method: Alamofire.HTTPMethod { return .get }
    
    var queryItems: [URLQueryItem]? {
        return [
            URLQueryItem(name: "limit", value: "\(limit)"),
            URLQueryItem(name: "offset", value: "\(offset)")
        ]
    }
    
}
