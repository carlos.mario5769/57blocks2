//
//  ApiResponse+PokemonResult.swift
//  test57Blocks
//
//  Created by Carlos on 5/09/21.
//

import Foundation

extension ApiResponse {
    struct PokemonResult: Codable {
        var count: Int?
        var results: [PokemonItem]?
    }
    
    struct PokemonItem: Codable {
        var name: String?
        var url: String?
    }
}
