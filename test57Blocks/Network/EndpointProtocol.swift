//
//  EndpointProtocol.swift
//  test57Blocks
//
//  Created by Carlos on 5/09/21.
//

import Foundation
import Alamofire

protocol EndpointProtocol: URLRequestConvertible {
    var path: String { get }
    var method: Alamofire.HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var params: Parameters? { get }
    var queryItems: [URLQueryItem]? {get}
}

extension EndpointProtocol {

    var baseURL: URL {
        return URL(string: "https://pokeapi.co/api/v2/")!
    }

    func encode(_ urlRequest: URLRequestConvertible) throws -> URLRequest {
        return try encoding.encode(urlRequest, with: params)
    }

    func asURLRequest() throws -> URLRequest {
        var urlComponent = URLComponents(string: baseURL.appendingPathComponent(path).absoluteString)!
        urlComponent.queryItems = queryItems
        var urlRequest = URLRequest(url: urlComponent.url!)
        urlRequest.httpMethod = method.rawValue
        if let headers = headers {
            urlRequest.headers = headers
        }
        headers?.forEach { urlRequest.setValue($0.value, forHTTPHeaderField: $0.name) }
        urlRequest = try encode(urlRequest)
        return urlRequest
    }

    var queryItems: [URLQueryItem]? {
        return nil
    }

    var headers: HTTPHeaders? {
        return [
        "Content-Type": "application/json"
        ]
    }

    var encoding: ParameterEncoding {
        return Alamofire.JSONEncoding.default
    }

    var params: Parameters? {
        return nil
    }
}

struct Endpoint {}
