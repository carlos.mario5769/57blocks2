//
//  Endpoint+GetPokemonDetail.swift
//  test57Blocks
//
//  Created by Carlos on 5/09/21.
//

import Foundation
import Alamofire

extension Endpoint {
    
    struct GetPokemonDetail {
        let pokemonId: String
    }
}

extension Endpoint.GetPokemonDetail: EndpointProtocol {

    var path: String { return "pokemon/\(pokemonId)" }
    
    var method: Alamofire.HTTPMethod { return .get }
    
}
