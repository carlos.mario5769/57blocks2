//
//  ApiClient.swift
//  test57Blocks
//
//  Created by Carlos on 5/09/21.
//

import Foundation
import Alamofire

class ApiClient: Session {
    private static var privateShared: ApiClient?

    public static var sharedInstance: ApiClient {
        get {
            guard let uwShared = privateShared else {
                let configuration = URLSessionConfiguration.ephemeral
                configuration.urlCache = URLCache.shared
                configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
                configuration.timeoutIntervalForRequest = 30
                configuration.timeoutIntervalForResource = 60
                privateShared = ApiClient(configuration: configuration)
                return privateShared!
            }
            return uwShared
        }
    }

    func doRequest<T: Decodable>(req: EndpointProtocol, dateFormat: String = "yyyy/MM/dd HH:mm:ss", completionHandler: @escaping ((ApiResult<T>) -> Void)) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        _ = self
            .request(req)
            .responseJSON(completionHandler: { response in
                var output: ApiResult<T>
                let statusCode = response.response?.statusCode
                guard let data = response.data else { return }
                do {
                    let decoder = JSONDecoder()
                    let value = try decoder.decode(T.self, from: data)
                    output = ApiResult.success(value, statusCode ?? 0)
                } catch let error {
                    output = ApiResult.failure(error, response.data, statusCode ?? 0)
                }
                completionHandler(output)

        })
    }
}

enum ApiResult<T> {
    case success(T, Int)
    case failure(Error, Data?, Int)
}
